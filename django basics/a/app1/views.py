from django.contrib import auth
from django.contrib.auth.models import User
from django.http.response import HttpResponse
from django.shortcuts import redirect, render
from django.http import HttpResponse

# Create your views here.


def home(request):
    return render(request, 'a.html', {'heading': ' Home Page', 'name': 'Kashish', 'portfolio': 'http://127.0.0.1:8000/profile', 'signup': 'signup', 'login': 'login', 'logout': 'logout'})


def profile(request):
    return render(request, 'a.html', {'heading': ' Basic Functionality Page', 'name': 'Kashish', 'portfolio': 'https://gitlab.com/kashish10'})


def expression(request):
    a = int(request.POST['text1'])
    b = int(request.POST['text2'])
    c = a + 2 * b
    return render(request, 'expression.html', {'result': c})


def signup(request):
    if request.method == "POST":
        username = request.POST['username']
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        email_id = request.POST['email_id']
        password = request.POST['password']
        if User.objects.filter(username=username).exists():
            print("USERNAME EXISTS !")
            return render(request, 'signup.html')
        else:
            x = User.objects.create_user(
                username=username, first_name=first_name, last_name=last_name, email=email_id, password=password)
            x.save()
            print("User Created")
            return redirect('/')
    else:
        return render(request, 'signup.html')


def login(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = auth.authenticate(username=username, password=password)
        if user is not None:
            auth.login(request, user)
            print(" ********Login Successful !******** ", user)
            return redirect('/')
        else:
            print(" ********Login Failed !******** ")
            return render(request, 'login.html')

    else:
        return render(request, 'login.html')


def logout(request):
    auth.logout(request)
    return redirect('/')
